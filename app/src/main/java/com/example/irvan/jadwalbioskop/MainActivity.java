package com.example.irvan.jadwalbioskop;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.irvan.jadwalbioskop.adapter.KotaAdapter;
import com.example.irvan.jadwalbioskop.model.Kota;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    RecyclerView rvKota;
    KotaAdapter kotaAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvKota = (RecyclerView)findViewById(R.id.rv_kota);
        rvKota.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        rvKota.setLayoutManager(new LinearLayoutManager(this));

        API api = API.Factory.create();

        Call<Kota> listKota = api.getListKota();
        listKota.enqueue(new Callback<Kota>() {
            @Override
            public void onResponse(Call<Kota> call, Response<Kota> response) {
                kotaAdapter = new KotaAdapter(MainActivity.this, response.body().getData());
                rvKota.setAdapter(kotaAdapter);
            }

            @Override
            public void onFailure(Call<Kota> call, Throwable t) {
                Log.d("TES", "GAGAL "+t.getMessage());
            }
        });
    }
}
