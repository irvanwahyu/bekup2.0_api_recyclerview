package com.example.irvan.jadwalbioskop.adapter;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.irvan.jadwalbioskop.DetailActivity;
import com.example.irvan.jadwalbioskop.R;
import com.example.irvan.jadwalbioskop.model.Film;
import com.example.irvan.jadwalbioskop.model.Jadwal;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by irvan on 10/23/2016.
 */

public class FilmAdapter extends RecyclerView.Adapter<FilmAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imgFilm;
        TextView judulFilm, genreFilm, durationFilm;

        public ViewHolder(View itemView) {
            super(itemView);
            imgFilm = (ImageView)itemView.findViewById(R.id.img_film_detail);
            judulFilm = (TextView)itemView.findViewById(R.id.txt_film_detail);
            genreFilm = (TextView)itemView.findViewById(R.id.txt_genre_detail);
            durationFilm = (TextView)itemView.findViewById(R.id.txt_duration_detail);
        }
    }

    private List<Film.Data> listFilm;
    private Context context;
    private JadwalAdapter jadwalAdapter;
    private RecyclerView rvJadwal;
    private Film.Data film;
    private Jadwal jadwal;

    public FilmAdapter(Context context) {
        this.context = context;
        //this.listFilm = listFilm;
        listFilm = new ArrayList<>();
    }

    private void add(Film.Data item) {
        listFilm.add(item);
        notifyItemInserted(listFilm.size() - 1);
    }

    public void addAll(List<Film.Data> listFilm) {
        for (Film.Data film : listFilm) {
            add(film);
        }
    }

    public Context getContext() {
        return context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.list_film, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inf = LayoutInflater.from(getContext());
                View v = inf.inflate(R.layout.alert_jadwal_main, null);

//                rvJadwal = (RecyclerView)v.findViewById(R.id.rv_jadwal);
//                rvJadwal.addItemDecoration(new DividerItemDecoration(v.getContext(), DividerItemDecoration.VERTICAL));
//                rvJadwal.setLayoutManager(new LinearLayoutManager(v.getContext()));
//
//                jadwalAdapter = new JadwalAdapter(v.getContext());
//
//                rvJadwal.setAdapter(jadwalAdapter);
//
//                jadwalAdapter.addAll(film.getJadwal());

                //Toast.makeText(getContext(), jadwal.getBioskop(), Toast.LENGTH_SHORT).show();

                builder.setView(v);

                AlertDialog ad = builder.create();
                ad.show();
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Film.Data film = listFilm.get(position);

        Picasso.with(getContext()).load(film.getPoster()).into(holder.imgFilm);
        holder.judulFilm.setText(film.getMovie());
        holder.genreFilm.setText(getContext().getResources().getString(R.string.genre, film.getGenre()));
        holder.durationFilm.setText(getContext().getResources().getString(R.string.duration, film.getDuration()));
    }

    @Override
    public int getItemCount() {
        return listFilm.size();
    }
}