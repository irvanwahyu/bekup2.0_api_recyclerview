package com.example.irvan.jadwalbioskop.model;

import java.util.List;

/**
 * Created by irvan on 10/24/2016.
 */

public class Jadwal {

    private String bioskop;
    private List<String> jam;
    private String harga;

    public String getBioskop() {
        return bioskop;
    }

    public String getHarga() {
        return harga;
    }

    public List<String> getJam() {
        return jam;
    }
}
