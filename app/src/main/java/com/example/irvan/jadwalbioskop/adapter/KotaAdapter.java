package com.example.irvan.jadwalbioskop.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.irvan.jadwalbioskop.DetailActivity;
import com.example.irvan.jadwalbioskop.R;
import com.example.irvan.jadwalbioskop.listener.ItemClick;
import com.example.irvan.jadwalbioskop.model.Kota;

import java.util.List;

/**
 * Created by irvan on 10/23/2016.
 */

public class KotaAdapter extends RecyclerView.Adapter<KotaAdapter.ViewHolder> {

    private ItemClick itemClick;

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView txtKota;

        public ViewHolder(View itemView) {
            super(itemView);

            txtKota = (TextView)itemView.findViewById(R.id.txt_kota);
        }
    }

    private List<Kota.Data> listKota;
    private Context context;

    public KotaAdapter(Context context, List<Kota.Data> listKota) {
        this.context = context;
        this.listKota = listKota;
    }

    public Context getContext() {
        return context;
    }

    @Override
    public KotaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.list_kota, parent, false);

        final ViewHolder viewHolder = new ViewHolder(view);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = viewHolder.getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    Intent intent = new Intent(getContext(), DetailActivity.class);
                    intent.putExtra(DetailActivity.ITEM_KOTA, listKota.get(position).getKota());
                    intent.putExtra(DetailActivity.ITEM_ID, listKota.get(position).getId());
                    getContext().startActivity(intent);
                }
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(KotaAdapter.ViewHolder holder, int position) {

        Kota.Data kota = listKota.get(position);

        holder.txtKota.setText(kota.getKota());
    }

    @Override
    public int getItemCount() {
        return listKota.size();
    }
}
