package com.example.irvan.jadwalbioskop.listener;

import android.view.View;

/**
 * Created by irvan on 10/23/2016.
 */

public interface ItemClick {

    void onItemClick(int position, View view);
}
