package com.example.irvan.jadwalbioskop.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.irvan.jadwalbioskop.R;
import com.example.irvan.jadwalbioskop.model.Jadwal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by irvan on 10/24/2016.
 */

public class JadwalAdapter extends RecyclerView.Adapter<JadwalAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtBioskop, txtHarga;
        LinearLayout lyJam;

        public ViewHolder(View itemView) {
            super(itemView);

            txtBioskop = (TextView)itemView.findViewById(R.id.txt_bioskop);
            txtHarga = (TextView)itemView.findViewById(R.id.txt_harga);
            lyJam = (LinearLayout) itemView.findViewById(R.id.lyTime);
        }
    }

    private List<Jadwal> listJadwal;
    private Context context;

    public JadwalAdapter(Context context) {
        this.context = context;
        listJadwal = new ArrayList<>();
    }

    private void add(Jadwal item) {
        listJadwal.add(item);
        notifyItemInserted(listJadwal.size() - 1);
    }

    public void addAll(List<Jadwal> listJadwal) {
        for (Jadwal jadwal : listJadwal) {
            add(jadwal);
        }
    }

    public Context getContext() {
        return context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.alert_jadwal_main, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Jadwal jadwal = listJadwal.get(position);

        holder.txtBioskop.setText(jadwal.getBioskop());

        for (int i = 0; i < jadwal.getJam().size(); i++){
            View view = LayoutInflater.from(getContext()).inflate(R.layout.alert_jadwal, holder.lyJam, false);
            TextView txtJam = (TextView)view.findViewById(R.id.txt_jam);

            txtJam.setText(jadwal.getJam().get(position));

            holder.lyJam.addView(view);
        }

        holder.txtHarga.setText(jadwal.getHarga());

    }

    @Override
    public int getItemCount() {
        return listJadwal.size();
    }
}
