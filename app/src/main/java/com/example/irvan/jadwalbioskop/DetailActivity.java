package com.example.irvan.jadwalbioskop;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.example.irvan.jadwalbioskop.adapter.FilmAdapter;
import com.example.irvan.jadwalbioskop.model.Film;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {

    public static final String ITEM_KOTA = "kota";
    public static final String ITEM_ID = "id";
    public static Context context;

    String kota, id;
    RecyclerView rvFilm;
    FilmAdapter filmAdapter;
    Film.Data film;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        id = getIntent().getStringExtra(ITEM_ID);
        kota = getIntent().getStringExtra(ITEM_KOTA);

        getSupportActionBar().setTitle(kota);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvFilm = (RecyclerView)findViewById(R.id.rv_film);
        rvFilm.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rvFilm.setLayoutManager(new LinearLayoutManager(this));

        filmAdapter = new FilmAdapter(this);

        rvFilm.setAdapter(filmAdapter);

        API api = API.Factory.create();

        Call<Film> listFilm = api.getListFilm(id);
        listFilm.enqueue(new Callback<Film>() {
            @Override
            public void onResponse(Call<Film> call, Response<Film> response) {
                //filmAdapter = new FilmAdapter(DetailActivity.this, response.body().getData());
                //rvFilm.setAdapter(filmAdapter);
                filmAdapter.addAll(response.body().getData());
            }

            @Override
            public void onFailure(Call<Film> call, Throwable t) {
                Log.d("TES", "GAGAL "+t.getMessage());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
